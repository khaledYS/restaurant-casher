import { createContext } from "react";

export const EmployeeContext = createContext(null);  
export const LoadingContext = createContext(null);
